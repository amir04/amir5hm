#include "Arrow.h"


Arrow::Arrow(const Point& a, const Point& b, const string& type, const string& name):Shape(name, type)
{
	this->_p1 = a;
	this->_p2 = b;
}

Arrow::~Arrow()
{
	//there is no memory to release
}

void Arrow::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char RED[] = { 255, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), RED, 100.0f).display(disp);
}
void Arrow::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), BLACK, 100.0f).display(disp);
}


double Arrow::getArea() const
{
	return 0;
}

double Arrow::getPerimeter() const
{
	//distance between 2 points
	return (_p1.distance(_p2));
}

void Arrow::move(const Point& other)
{
	_p1 = _p1 + other;
	_p2 = _p2 + other;
}

