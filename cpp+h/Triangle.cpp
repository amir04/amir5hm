#include "Triangle.h"

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name) :Polygon(type, name)
{
	this->_a = a;
	_points.push_back(a);
	this->_b = b;
	_points.push_back(b);
	this->_c = c;
	_points.push_back(c);
}

Triangle::~Triangle()
{
	/*there is no memory to release*/
}

void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}


double Triangle::getArea() const
{
	return 0.5*abs(_a.getX()*(_c.getY() - _b.getY()) + _b.getX()*(_a.getY() - _c.getY()) + _c.getX()*(_b.getY() - _a.getY()));
}

double Triangle::getPerimeter() const
{
	return (_a.distance(_b) + _b.distance(_c) + _c.distance(_a));
}

void Triangle::move(const Point& other)
{
	_points.pop_back();
	_points.pop_back();
	_points.pop_back();
	_a = _a + other;
	_b = _b + other;
	_c = _c + other;
	_points.push_back(_a);
	_points.push_back(_b);
	_points.push_back(_c);
}