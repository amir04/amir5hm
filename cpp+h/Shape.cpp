#include "Shape.h"

Shape::Shape(const string& name, const string& type)
{
	this->_name = name;
	this->_type = type;
}
/*
virtual double getArea() const = 0;
virtual double getPerimeter() const = 0;
virtual void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board) = 0;
virtual void move(const Point& other) = 0; // add the Point to all the points of shape
void printDetails() const;
*/
void Shape::printDetails() const
{
	printf("%s, %s, %.2f, %.2f", getType(), getName(), getArea(), getPerimeter());
}
string Shape::getType() const
{
	return this->_type;
}

string Shape::getName() const
{
	return this->_name;
}
