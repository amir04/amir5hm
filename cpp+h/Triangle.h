#pragma once
#include "Polygon.h"

class Triangle : public Polygon
{
private:
	Point _a;
	Point _b;
	Point _c;

public:
	Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name);
	virtual ~Triangle();
	virtual void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
	virtual void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);

	virtual double getArea() const;
	virtual double getPerimeter() const;
	virtual void move(const Point& other); // add the Point to all the points of shape
	
	// override functions if need (virtual + pure virtual)
};